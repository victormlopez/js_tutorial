const debug = require('debug')('app:controllers:v1:index');

const ExampleService = require('../../services/v1/example');

const Sequelize = require('sequelize');

const sequelize = new Sequelize('test', 'visitor', '', {
  host: 'slashweb.cyuazzw9rdsu.us-east-1.rds.amazonaws.com',
  dialect: 'mysql',

  pool: {
    max: 5,
    min: 0,
    acquire: 30000,
    idle: 10000,
  },
});

const User = sequelize.define('user', {
  username: Sequelize.STRING,
  firstName: Sequelize.STRING,
  lastName: Sequelize.STRING,
});

const ExampleController = {

  index: async (req, res) => {
    debug('executing index action');

    const example = await ExampleService.example();
    res.status(200).send(example);
  },
  database: async (req, res) => {
    let usersAdded = [1, 2, 3, 4];
    let usersFinal = [];
    usersAdded = usersAdded.map((user) => {
      return {
        username: 'Usuario ' + user,
        firstName: 'Firstname ' + user,
        lastName: 'Lastname ' + user,
      }
    })

    await sequelize.sync();
    usersAdded.forEach((user) => {
      const newuser = User.create(user);
      usersFinal.push(newuser);
    })

    Promise.all(usersFinal)
      .then((result) => {
        return res.status(200).send(result);
      });
  },

};

module.exports = ExampleController;
